//import * as data from '../../configs/WebServiceConfig.json';
//Not used at the moment 
import base64 from 'react-native-base64';

export default class ApiService{
 
    static _postCall = async (url = ``, data = {}, email = null, password = null) =>{
        try {
            let credentials;
            if(email != null && password != null){ 
                credentials = "Basic " + base64.encode(email + ":" + password);
            }else{
                credentials = "";
            }
            console.log("Before Call: "+ data +"   "+credentials);
            let response = await fetch(url,
                {          
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                //mode: "cors", // no-cors, cors, *same-origin
                //cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                //credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": credentials, 
                    // "Content-Type": "application/x-www-form-urlencoded",
                },
                //redirect: "follow", // manual, *follow, error
                //referrer: "no-referrer", // no-referrer, *client
                body: JSON.stringify(data)
                }
            );
        if(response.status == 200){
            let responseJson = await response.json();
            return responseJson;
        } else {
            throw new Error(`Webservice call was not successful and returned the following status code: ${response.status}`);
        }
        } catch (error) {
            if(__DEV__){
                //console.error(error);
            }
        }
    }

    /*
    static _postCall = async (url = ``, data = {}) =>{
        try {
            let response = await fetch(url,
                {          
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                mode: "cors", // no-cors, cors, *same-origin
                cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    // "Content-Type": "application/x-www-form-urlencoded",
                },
                redirect: "follow", // manual, *follow, error
                referrer: "no-referrer", // no-referrer, *client
                body: JSON.stringify(data),
                }
            );
        if(response.status == 200){
            let responseJson = await response.json();
            return responseJson;
        } else {
            throw new Error(`Webservice call was not successful and returned the following status code: ${response.status}`);
        }
        } catch (error) {
        console.error(error);
        }
    }
    */

    static _deleteCall = async (url = ``, data = {}) =>{
        try {
            let response = await fetch(url,
                {          
                method: "DELETE", // *GET, POST, PUT, DELETE, etc.
                mode: "cors", // no-cors, cors, *same-origin
                cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    // "Content-Type": "application/x-www-form-urlencoded",
                },
                redirect: "follow", // manual, *follow, error
                referrer: "no-referrer", // no-referrer, *client
                body: JSON.stringify(data),
                }
            );
        if(response.status == 200){
            let responseJson = await response.json();
            return responseJson;
        } else {
            throw new Error(`Webservice call was not successful and returned the following status code: ${response.status}`);
        }
        } catch (error) {
        console.error(error);
        }
    }
}
