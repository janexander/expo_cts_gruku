import {SecureStore} from 'expo';

export default class KeyStore{

    static _setPassword = async (key = "key_pw", value_pw) => {
        if(__DEV__){
            console.log(value_pw);
        }
        await SecureStore.setItemAsync(key, value_pw);    
    };

    static _setEmail = async (key = "key_email", value_email) => {
        if(__DEV__){
            console.log(value_email);
        }
        await SecureStore.setItemAsync(key, value_email);    
    };
    
    static _getPassword = async (key = "key_pw") => {
        try{
            let value_pw = await SecureStore.getItemAsync(key)

            return value_pw;

        }catch(error){
            console.error(error)
        }
      };

    static _getEmail = async (key = "key_email") => {
        try{
            let value_email = await SecureStore.getItemAsync(key)
        
            return value_email;

        }catch(error){
            console.error(error)
        }
    };

    static _deleteCredentials = async() => {
        if(__DEV__){
            let logout;
            try{    
            logout = await SecureStore.deleteItemAsync("key_pw");
            console.debug(logout.stringify());
            logout = await SecureStore.deleteItemAsync("key_email");
            console.debug(logout.stringify());
            }catch(error){
                console.error(error);
            }
        }
        else{   
            try{
            logout = await Expo.SecureStore.deleteItemAsync("key_pw");
            logout = await Expo.SecureStore.deleteItemAsync("key_email");
            }catch(error){
                alert("The applicaton wasn't able to delete the user data!");
            }
        }
    }
}