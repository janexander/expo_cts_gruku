import KeyStore from '../Datacomponents/KeyStore';
import ApiService from '../Datacomponents/ApiService';
import moment from 'moment';

export const getClasses = async() => {
    
    try{

        console.log("In getClasses");
        preSet_Email = await KeyStore._getEmail();
        preSet_Password = await KeyStore._getPassword();
        console.log(preSet_Email);
        console.log(preSet_Password);    

    }catch(error){
        console.error(error);
    }    
    try{

        const startDate = moment().subtract(30, 'days').format('DD-MM-YYYY');
        const endDate = moment().add(15, 'days').format('DD-MM-YYYY');

        let responseJson = await ApiService._postCall('http://group-fitness.ch/ManagementSysRest/rest/classrunstats/classrunsindaterange',
        {
            "startDate":`${startDate}`,
            "endDate":`${endDate}`
        }, preSet_Email, preSet_Password);

        if(__DEV__){
            //console.log(responseJson);
        }

        return responseJson;

    }catch(error){
        console.log(error);
    }
}

