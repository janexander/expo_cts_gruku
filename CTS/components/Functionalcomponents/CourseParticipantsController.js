import KeyStore from '../Datacomponents/KeyStore';
import ApiService from '../Datacomponents/ApiService';

export const setCourseParticipants = (classId, courseParticipantNumber) => {
    
    try{

        preSet_Email = KeyStore._getEmail();
        preSet_Password = KeyStore._getPassword();
            
    }catch(error){
        console.error(error);
        
        try{
            let responseJson = ApiService._postCall('group-fitness.ch/ManagementSysRest/rest/classrunstats',
            {
            
                "classRunId":`${classId}`, 
                "customerAmount":`${courseParticipantNumber}`
            
            }, preSet_Email, preSet_Password);

            if(__DEV__){
                console.log(responseJson);
            }

            return responseJson;

        }catch(error){
            console.log(error);
        }
        return error;
    }
}

