import KeyStore from '../Datacomponents/KeyStore';
import ApiService from '../Datacomponents/ApiService';

const login = async (email, password) => {
   
    let responseJson;

    try{
        
        responseJson = await ApiService._postCall('http://group-fitness.ch/ManagementSysRest/rest/personAuth',
        {
            "email" : `${email}`,
            "password" : `${password}`
        });

    }catch(error){
        if(__DEV__){
            //console.log(error);
        }
    }
    
    if(responseJson.authenticated){

    KeyStore._setEmail(email);
    KeyStore._setPassword(password);

    return responseJson;

    }else{
        return responseJson;
    }
}

export default login;
