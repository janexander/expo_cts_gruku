import React from 'react';
import {
  Button,
  StyleSheet,
  TextInput,
  View,
  Text,
  Alert,
} from 'react-native';
import login from '../Functionalcomponents/LoginController';
import ValidationComponent from 'react-native-form-validator';
//import _alert from '../Graphicalcomponents/Alert';


export default class LoginForm extends ValidationComponent {
 
    constructor(props) {
        super(props);
        this.state = {
          email: "",
          password: "",
        }
    }

  render(){
    return (
      <View style={styles.mainviewStyle}>
        <TextInput
          style={styles.input}
          onChangeText={(email) => this.setState({email})}
          placeholder="E-Mail"
        />
        <TextInput
          style={styles.input}
          onChangeText={(password) => this.setState({password})}
          secureTextEntry={true}
          placeholder="Password"
        />
        <Button
          onPress={() => this._onSubmit()}
          title="Submit"
          color="#bc3c4c"
        />
      </View>
    )
  }

  async _onSubmit(){
    // press button
      try{
          this.validate({
            email: {email: true, required: true, },
            password: {required: true},  
          });

          if(this.errors.length > 0){
            throw new Error("unvalid credentials")
          }

          try{
            //Call LoginController
              console.log("Call login function");
              let responseJson = await login(this.state.email, this.state.password);
              if(responseJson.authenticated){
                this.props.navigation.navigate('Main');
              }else{
                Alert.alert(responseJson.message);
              }
          }catch(error){
            Alert.alert("Es besteht aktuell ein Fehler mit der Verbindung!")
            //_alert("Unable to login", error, "", undefined);
          }

        }catch(error){
            //console.error(this.getErrorMessages());
            Alert.alert(this.getErrorMessages());
            //_alert("Error", error, "", undefined);
        }
    }
}
var styles = StyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
  paddingTop: 100,
  },
  input: {
    width: 250,
    margin: 5
  },
});