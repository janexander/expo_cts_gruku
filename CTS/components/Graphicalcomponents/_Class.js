import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Button,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

export default class _Class extends React.Component {

  constructor(props){
    super(props)
    this.state={
        classChanged: false,
        classDetail: this.props.classDetails,
    }
  }

  render() {
    return (
      <View> 
        <Button 
        title="Kurs"
        onPress={() => this._handlePress()}
        />
        <Text>{this.state.classDetail.classRuns}</Text>
      </View>
    );
  }

  _handlePress(){
      this.props.navigation.navigate('CourseParticipants',{
      classId:  this.state.classDetail["id"] ,
      amountCustomer:  this.state.classDetail["amountCustomer"],
    });
  }
}