import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import _Class from './_Class';
import {getClasses} from '../Functionalcomponents/ClassController';

export default class ClassList extends React.Component {

  constructor(props){
    super(props)
    this.state={
      Classes: null,
    }
  }

  componentWillMount(){
   this.setState({Classes: this.loadClasses()})
  }

  render() {
    return this.state.Classes instanceof Object ? (
        <View> 
          <_Class 
            classDetails={this.state.Classes}
            navigation={this.props.navigation}
          />
        </View>
      ) : <Text>Keine Kurse gefunden!</Text>;
  }

  async loadClasses(){
    let responseJson = await getClasses();
    console.log(responseJson);
    return responseJson;
  }
}