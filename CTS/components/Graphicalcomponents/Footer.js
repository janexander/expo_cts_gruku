import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  
} from 'react-native';

export default class Footer extends Component{
    constructor(props) {
        super(props);
        this.state = {
            footerState: "",
        }
    }
    
    
    render(){
        return (
            <View style={stylesFooter.footer}>
            <TouchableHighlight style={stylesFooter.bottomButtons}>
                <Text style={stylesFooter.footerText}>{this.props.footer}</Text>
            </TouchableHighlight>
            </View>
        )
    }
}

var stylesFooter = StyleSheet.create({
  footer: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor:'#bc3c4c',
    flexDirection:'row',
    height:60,
    alignItems:'center',
  },
  bottomButtons: {
    alignItems:'center',
    justifyContent: 'center',
    flex:1,
  },
  footerText: {
    color:'white',
    fontWeight:'bold',
    alignItems:'center',
    fontSize:18,
  },
  textStyle: {
    alignSelf: 'center',
    color: 'orange'
  },
  scrollViewStyle: {
    borderWidth: 2,
    borderColor: 'blue'
  }
  });
  