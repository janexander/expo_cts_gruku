import React from 'react';
import {
    Alert,
} from 'react-native';

const _alert = Alert.alert(
    title,
    message,
        [
          {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
    { cancelable: false }
);

export default _alert;
