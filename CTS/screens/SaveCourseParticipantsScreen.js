import React from 'react';
import {
  TextInput,
  Platform,
  Button,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import Footer from '../components/Graphicalcomponents/Footer';
import {setCourseParticipants} from '../components/Functionalcomponents/CourseParticipantsController';
import ValidationComponent from 'react-native-form-validator';

export default class SaveCourseParticipantsScreen extends ValidationComponent {

  constructor(props){
    super(props);
    this.state = {
      classId: this.props.navigation.getParam('classId'),
      customers: this.props.navigation.getParam('amountCustomer'),
    }
  }

  render() {
    return (
        <View  style={styles.container}>
            <View style={styles.container}>
                <Text>
                    Geben Sie die Anzahl Kursbesucher an:
                </Text>
                <TextInput
                  placeholder={this.state.customers}
                />
                <Button
                onPress={() => this._onSubmit()}
                title="Speichern"
                color="#bc3c4c"
                />
            </View>
            <View>
              <Footer />
            </View>
      </View>
    );
  }

  _onSubmit(){
    try{
        this.validate({
          classId: { required:true, numbers:true, },
        });
    
        setCourseParticipants(this.state.classId, this.state.customers);
        this.props.navigation.goBack();

    }catch(error){
        //console.error(this.getErrorMessages());
        Alert.alert(this.getErrorMessages());
        //_alert("Error", error, "", undefined);
    }  
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#333333',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
