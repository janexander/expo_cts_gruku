import React from 'react';
import {
  StyleSheet,
  View, 
} from 'react-native';
import Footer from '../components/Graphicalcomponents/Footer';
import LoginForm from '../components/Graphicalcomponents/LoginForm';

export default class LoginScreen extends React.Component {
  /*static navigationOptions = {
    header: null,
  };*/

  constructor(props) {
    super(props);
    this.state={
      footer: "CTS GruKu - Login",
    }
  }

  render(){
    return (
      <View style={styles.mainviewStyle}>
        <LoginForm navigation={this.props.navigation}/>
        <Footer footer={this.state.footer} />
      </View>
    )
  }
}
var styles = StyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
  },
  input: {
    width: 250,
    margin: 5
  },
});