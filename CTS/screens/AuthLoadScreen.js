import React from 'react';
import {
  ActivityIndicator,
  Button,
  StyleSheet,
  TextInput,
  View,
  StatusBar,
} from 'react-native';
import KeyStore from '../components/Datacomponents/KeyStore';

export default class AuthLoadScreen extends React.Component {
    constructor(props) {
      super(props);
      this.alreadyLoggedInCheck();
    }
  
    async alreadyLoggedInCheck(){
        const userEmail = await KeyStore._getEmail();
        const userPassword = await KeyStore._getPassword();
        console.log(userEmail);
        console.log(userPassword);
        try {
            this.props.navigation.navigate((userEmail && userPassword) ? 'Auth' : 'Main');
        } catch (error) {
            console.error(error);
        }
    }

    render() {
      return (
        <View>
          <ActivityIndicator />
          <StatusBar barStyle="default" />
        </View>
      );
    }
  }
  