import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator} from 'react-navigation';

import LoginScreen from '../screens/LoginScreen';

export default LoginStack = createStackNavigator({
  Login: LoginScreen, 
});

