import React from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import {HomeStack} from './MainAppStack';
import LoginAppStack from './LoginAppStack';
import AuthLoadStack from './AuthLoadStack';

export default createSwitchNavigator(
  {
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  Main: HomeStack,
  Auth: LoginAppStack,
  AuthLoad: AuthLoadStack,
  },
  {
    initialRouteName: 'Auth'
  }
);