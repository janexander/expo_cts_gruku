import React from 'react';
import { Platform } from 'react-native';
import {createStackNavigator} from 'react-navigation';

import ClassOverviewScreen from '../screens/ClassOverviewScreen';
import SaveCourseParticipantsScreen from '../screens/SaveCourseParticipantsScreen';

export const HomeStack = createStackNavigator({
  Home: ClassOverviewScreen,
  CourseParticipants: SaveCourseParticipantsScreen,
});

