**Starting the App**

Die Installation der Expo SDK bzw. der Quick Start ist unter folgendem Link beschrieben:

https://docs.expo.io/versions/v31.0.0/introduction/installation

Darüber hinaus is eine Node.js Installation notwendig.


Um die App zu starten gibt es verschiedene Möglichkeiten, wobei diese nachfolgend beschrieben sind:

https://docs.expo.io/versions/v31.0.0/guides/up-and-running.html
